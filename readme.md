### API Examples


## Rules
everything is converted to lowercase
anything inside of eval ( ) is validated to be floats or integers
assume measurements to be standardized, only accept values


## Operators
OR ~
AND ,
NOT !
key/value delimiter :
range delimiter -
wildcard $
eval [ ]
grouping ( )

## Reserved Names
# Done - Need unit tests
fields
filter
sort
greater_than
less_than
range
limit
offset
asc
desc
random
elements | element
groups | group
blocks | block
periods | period
phases | phase

# WIP
categories | category


## Measurement Standards
Density = g/cm^3
Temperature = K


## Examples

# Base_path
//api.all-the-things.com/pt/v1/

# All phases
{ base_path }/phases

# Selects one random block
{ base_path }/blocks/random

# Selects two random blocks
{ base_path }/blocks/random?limit=2

# Select all elements
{ base_path }/elements

# Select the element where the name or symbol or atomic_number matches
{ base_path }/elements/Au
#"SELECT * FROM elements WHERE name = 'Au' OR symbol = 'Au' OR atomic_number = 'Au'"

# Only 20 elements starting with the 5th entry
{ base_path }/elements?limit=20&offset=5

# All elements with fields (name, number) sorted by name ascending then number descending
{ base_path }/elements?fields=name,atomic_number&sort=name:asc,atomic_number:desc

# All elements with fields (name, number) filtered by atomic numbers greater than 20
{ base_path }/elements?fields=name,atomic_number&filter=atomic_number:greater_than[20]

# All elements with fields (name, number) filtered by atomic numbers between 20 and 200
{ base_path }/elements?fields=name,atomic_number&filter=atomic_number:range[20-200]

# All elements within the "sharp" block
{ base_path }/elements?filter=block:sharp

# All elements within a random phase
{ base_path }/elements

# All elements within a category that has the string "metal" or "gas" in it
{ base_path }/elements?filter=category:%metal%~%gas%

# All elements categorized by "Actinoids"
{ base_path }/elements?filter=category:Actinoids,block:diffuse

# All elements categorized by (both "alkali metals" and "metalloids") OR Actinoids
{ base_path }/elements?filter=category:(alkali%20metals,metalloids)~Actinoids

# All elements categorized by anything but "metalloids"
{ base_path }/elements?filter=category:!metalloids

# Potential future usage and functionality
/phases?filter=Element.atomic_number:2

/elements?filter=category:Alkali%20Metals&fields=group


# Returns

# Elements
{
    "elements": [
        {
            "name":"Gold",
            "symbol":"Au",
            "atomic_number":"79",
            "stable":"true",
            "density":"19.30",
            "atomic_weight_uncertainty":"5",
			"atomic_weight":"196.966569",
            "melting_point":"1337.33",
            "boiling_point":"3129",
            "block": {
            	"name":"diffuse",
            	...
            },
            "period":"6",
            "group":"11",
            "categories": [
            	{
            		"name":"transistion metals",
            		...
            	},
            	...
            ],
            "phase": {
            	"name": "solid",
            	...
        	}
        },
        ...
    ]
}

# Blocks
{
    "blocks": [
        {
            "name":"sharp",
            "abbr":"s",
            "status":"confirmed",
            "color":"#532c2a"
        }
        ...
    ]
}

# Phases
{
    "phases": [
        {
            "name":"gas"
        }
        ...
    ]
}

# Groups
{
    "groups": [
        "1",
        "2",
        ...
    ]
}

# Periods
{
    "periods": [
        "1",
        "2",
        ...
    ]
}

# Categories
{
    "categories": [
        {
            "name":"Alkali metals",
            "color":"#ff6666"
        }
        ...
    ]
}

# TODO
# Populate elements_categories pivot table with test data
# Hook up categories filter
# Develop filter reversals for elements (e.g. /phases?filter=Element:Au)
# Hook up field alias functionality (e.g. /elements/Au?fields=phase)
# Scrape actual data
# Hook up Mashape Authentication