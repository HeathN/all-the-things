<?php

class Debug {

	/**
	 * Decides whether to show the debug based on config settings
	 *
	 * @return static method
	 */
	public static function __callStatic($name, $arguments)
	{
		if (Config::get('app.user_debug') && Config::get('app.debug'))
		{
			return forward_static_call_array(array('self', 'custom_'.$name), $arguments);
		}
	}

	/**
	 * Shows the last query ran in a format specified
	 *
	 * @param $return_type string|boolean
	 * @return string
	 */
	public static function custom_showLastQuery($return_type = false)
	{
		$queries = DB::getQueryLog();
		$query = end($queries);

		if ($return_type == 'dd')
		{
			return dd($query);
		}
		elseif ($return_type == 'var_dump')
		{
   			return var_dump($query);
		}
		else
		{
			return $query;
		}
	}
}