<?php

use Illuminate\Database\Migrations\Migration;

class DisjointCategoryAssignments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('elements', function($t)
		{
			$t->dropColumn('category_id');
		});

		Schema::create('elements_categories', function($t)
		{
			$t->increments('id');
			$t->integer('element_id');
			$t->integer('category_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// ah screw it
	}

}