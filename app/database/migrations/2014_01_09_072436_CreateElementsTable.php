<?php

use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('elements', function($t){
			$t->increments('id');
			$t->string('name');
			$t->string('symbol');
			$t->integer('atomic_number');
			$t->float('density');
			$t->float('atomic_weight');
			$t->integer('atomic_weight_uncertainty');
			$t->float('melting_point');
			$t->float('boiling_point');

			//Reference Variables
			$t->integer('block_id');
			$t->integer('period_id');
			$t->integer('group_id');
			$t->integer('category_id')->nullable();
			$t->integer('phase_id');
		});

		Schema::create('groups', function($t){
			$t->increments('id');
		});

		for ($i=0;$i<18;$i++)
		{
			Group::create(array());
		}

		Schema::create('periods', function($t){
			$t->increments('id');
		});

		for ($i=0;$i<7;$i++)
		{
			Period::create(array());
		}

		Schema::create('blocks', function($t){
			$t->increments('id');
			$t->string('name');
			$t->string('abbr');
			$t->string('status');
			$t->string('color');
		});

		$blocks = array(
			array('name' => 'sharp', 'abbr' => 's', 'status' => 'confirmed', 'color' => '#532c2a'),
			array('name' => 'principal', 'abbr' => 'p', 'status' => 'confirmed', 'color' => '#e29b64'),
			array('name' => 'diffuse', 'abbr' => 'd', 'status' => 'confirmed', 'color' => '#33445c'), 
			array('name' => 'fundamental', 'abbr' => 'f', 'status' => 'confirmed', 'color' => '#f5ca5c'), 
			array('name' => '', 'abbr' => 'g', 'status' => 'hypothetical', 'color' => '#ffffff')
		);

		foreach ($blocks as $block)
		{
			Block::create($block);
		}

		Schema::create('categories', function($t){
			$t->increments('id');
			$t->string('name');
			$t->string('color');
		});

		$categories = array(
			array('name' => 'Alkali metals', 'color' => '#ff6666'),
			array('name' => 'Alkaline earth metals', 'color' => '#ffdead'),
			array('name' => 'Noble gases', 'color' => '#c0ffff'),
			array('name' => 'Lanthanoids', 'color' => '#ffbfff'),
			array('name' => 'Actinoids', 'color' => '#ff99cc'),
			array('name' => 'Transistion metals', 'color' => '#ffc0c0'),
			array('name' => 'Poor metals', 'color' => '#cccccc'),
			array('name' => 'Metalloids', 'color' => '#cccc99'),
			array('name' => 'Polyatomic nonmetals', 'color' => '#a1ffc3'),
			array('name' => 'Diatomic nonmetals', 'color' => '#e7ff8f'),
		);

		foreach ($categories as $category)
		{
			Category::create($category);
		}

		Schema::create('phases', function($t){
			$t->increments('id');
			$t->string('name');
		});

		$phases = array(
			array('name' => 'gas'),
			array('name' => 'liquid'),
			array('name' => 'solid'),
			array('name' => 'plasma'),
			array('name' => 'Bose-Einstein condensate'),
		);

		foreach ($phases as $phase)
		{
			Phase::create($phase);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('elements', function($t){
			$t->drop();
		});

		Schema::table('groups', function($t){
			$t->drop();
		});

		Schema::table('categories', function($t){
			$t->drop();
		});

		Schema::table('periods', function($t){
			$t->drop();
		});

		Schema::table('phases', function($t){
			$t->drop();
		});

		Schema::table('blocks', function($t){
			$t->drop();
		});
	}

}