<?php

class Helpers {

	/**
	 * Strips common operators for sql usage
	 *
	 * @return string
	 */
	public static function stripOperators($string)
	{
		return str_replace('!', '', $string);
	}

	
}