<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $id
	 * @return Response
	 */
	public function show($id = null)
	{
		$controller = get_called_class();

		$model = $controller::$model;

		$query = $model::query();

		if (!empty($id))
       	{
       		$query = $model::byId($id, $query);
       	}

   		// Handle filters
   		if (Input::has('filter'))
   		{
   			$query = $model::addFilters($query);
   		}

   		// Handle fields
		if (Input::has('fields'))
		{
			$query = $model::addFields($query);
		}

		// Handle sorts
		if (Input::has('sort'))
		{
			$query = $model::addSorts($query);
		}

		// Handle limit
		if (Input::has('limit'))
		{
			$query = $query->take(Input::get('limit'));
		}

		// Handle offset
		if (Input::has('offset'))
		{
			$query = $query->skip(Input::get('offset'));
		}

		$results = $query->get();

		if (!empty($model::$attachables))
		{
			$i=0;
			foreach ($results as $result)
			{
				foreach ($model::$attachables as $column => $value)
				{
					// Model of the special filter cases
   					$filter_model = studly_case($column);
   					$attachable = $filter_model::byId($value)->first();

   					if ($attachable->id == $result->{$column.'_id'})
   					{
   						$result->$column = $attachable->toArray();
   					}
   					else
   					{
   						unset($results[$i]);
   					}
				}
				$i++;
			}
		}

       	if (!$results->count())
       	{
       		return App::abort(204);
       	}

       	echo $results->toJson();
	}

	public function __destruct()
	{
		

	}
}