<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// {element_id} name, symbol, atomic_number
Route::get('elements/{element_id?}', 'ElementController@show')->where('element_id', '[A-z\d]+');
// {block_id} name, abbr
Route::get('blocks/{block_id?}', 'BlockController@show')->where('block_id', '[A-z\d]+');
// {phase_id} name
Route::get('phases/{phase_id?}', 'PhaseController@show')->where('phase_id', '[A-z\d]+');
// {category_id} name
Route::get('categories/{category_id?}', 'CategoryController@show')->where('category_id', '[A-z\d]+');
// {period_id} id
Route::get('periods/{period_id?}', 'PeriodController@show')->where('block_id', '[A-z\d]+');
// {group_id} id
Route::get('groups/{group_id?}', 'GroupController@show')->where('group_id', '[A-z\d]+');