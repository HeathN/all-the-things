<?php

class Element extends Base {
	public $timestamps = false;
	protected $table = 'elements';
	protected $guarded = array();

	public static $rules = array();

	protected $hidden = array('id', 'block_id', 'phase_id', 'group_id', 'period_id');

	/*
	 * ID Columns - key columns used in explicit requests
	 */
	public static $id_columns = array('name', 'symbol', 'atomic_number');

	/*
	 * Allowable filters
	 */
	public static $allowable_filters = array('phase', 'category', 'group', 'period', 'block');
}
