<?php

class Phase extends Base {
	public $timestamps = false;
	protected $guarded = array();

	public static $rules = array();

	protected $hidden = array('id');

	/*
	 * ID Columns - key columns used in explicit requests
	 */
	public static $id_columns = array('name');
}
