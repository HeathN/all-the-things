<?php

class Period extends Base {
	public $timestamps = false;
	protected $guarded = array();

	public static $rules = array();

	/*
	 * ID Columns - key columns used in explicit requests
	 */
	public static $id_columns = array('id');
}
