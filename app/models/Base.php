<?php 

class Base extends Eloquent
{
	public static $attachables = array();

	/**
	 * Parse all the query filters
	 *
	 * @param Query Object $query
	 * @return Collection
	 */
	public static function addFilters($query)
	{
   		if ($filter_string = Input::get('filter'))
   		{
   			$groups = preg_match_all('/\(([^\)]+)\)/', $filter_string, $groups)
	   			? $groups[1]
   				: array($filter_string);

   			foreach ($groups as $group)
   			{
	   			// Separate the groups delimited by the OR operator
	   			$filter_or_groups = explode('~', $group);

	   			foreach ($filter_or_groups as $filter_or_group)
	   			{
	   				$query = self::assemble_query($query, $filter_or_group, 'OR');

	   				// Check to see if we should even handle AND clauses
					if (strpos($filter_or_group, ','))
					{
	   					$filter_and_groups = explode(',', $filter_or_group);
		
	   					foreach ($filter_and_groups as $filter_and_group)
	   					{
		   					$query = self::assemble_query($query, $filter_and_group, 'AND');
				   		} // ends the AND loop
				   	}
			   	} // ends the OR loop
		   	}
	   	}

	   	return $query;
	}

	/**
	 * Assemble all the components of the query
	 * This function is typically ran multiple times in one request
	 *
	 * @param Query Object $query
	 * @param string $filter_group A string to be parsed
	 * @param string $type The type of SQL clause operator
	 * @return Query Object
	 */
	private static function assemble_query($query, $filter_group, $type)
	{
		// Model of the requested record type
		$access_model = get_called_class();

		$override_default = false;

   		$filter_params = explode(':', $filter_group);
   		
   		$column = $filter_params[0];
   		$value = $filter_params[1];

   		// Model of the special filter cases
   		$filter_model = studly_case($column);

   		if (class_exists($filter_model) && in_array($column, $access_model::$allowable_filters))
   		{
   			static::$attachables[$column] = $value; 
   			return $query;
   		}

   		$uses_wildcard = strpos($value, '$') !== false;
   		$uses_not = strpos($value, '!') !== false;
   		$uses_compare = preg_match('/^(greater_than|less_than|range)\[([\.\d]+)(?:-([\.\d]+))?\]$/', $value, $compare_matches);

		$operator = '';
		
   		if ($uses_wildcard)
   		{
   			// Convert our URL wildcard to SQL wildcard syntax
   			$value = str_replace('$', '%', $value);

   			if ($uses_not)
   			{
   				$column = Helpers::stripOperators($value);
   				$operator .= 'NOT ';
   			}

   			$operator .= 'LIKE';
   		}
   		elseif ($uses_not)
   		{
   			$column = Helpers::stripOperators($value);
   			$operator = '<>';
   		}
   		else
   		{
   			$operator = '=';
   		}

   		if ($uses_compare)
   		{
   			$value = $compare_matches[2];

   			if ($compare_matches[1] == 'greater_than')
   			{
   				$operator = '>';
   			}
   			elseif ($compare_matches[1] == 'less_than')
   			{
   				$operator = '<';
   			}
   			elseif ($compare_matches[1] == 'range')
   			{
   				$range_params = array($compare_matches[2], $compare_matches[3]);

   				if ($type == 'AND')
   				{
   					$query = $query->whereBetween($column, $range_params);
   				}
   				elseif ($type == 'OR')
	   			{
	   				$query = $query->orWhereBetween($column, $range_params);
				}

   				$override_default = true;
   			}
   		}

   		if (!$override_default)
   		{
   			if ($type == 'AND')
   			{
   				$query = $query->where($column, $operator, $value);
   			}
   			elseif ($type == 'OR')
   			{
   				$query = $query->orWhere($column, $operator, $value);
   			}
   		}

   		return $query;
	}

	/**
	 * Parse all the query sorts
	 *
	 * @param Query Object $query
	 * @return Query Object
	 */
	public static function addSorts($query)
	{
   		if ($sort_string = Input::get('sort'))
   		{
   			$sort_groups = explode(',', $sort_string);

   			if (in_array('random', $sort_groups))
   			{
   				return $query->orderBy(DB::raw('RAND()'));
   			}

   			foreach ($sort_groups as $sort_group)
   			{
   				$column = $sort_group;
		   		$direction = Config::get('app.default_sort_direction');

   				if (strpos($sort_group, ':'))
   				{
		   			$sort_params = explode(':', $sort_group);

		   			$column = $sort_params[0];

		   			if (in_array($sort_params[1], array('asc', 'desc')))
		   			{
			   			$direction = $sort_params[1];
			   		}
		   		}

		   		$query = $query->orderBy($column, $direction);
		   	}
	   	}

	   	return $query;
	}

	/**
	 * Parse all the query fields
	 *
	 * @param Query Object $query
	 * @return Query Object
	 */
	public static function addFields($query)
	{
		// Handle fields
		if ($fields_string = Input::get('fields'))
		{
			$fields = explode(',', $fields_string);
			
			return $query->select($fields);
		}

		return $query;
	}

	/**
	 * Grab a single result based on an key
	 *
	 * @param Query Object $query
	 * @param string|int $id
	 * @return Collection
	 */
	public static function byId($id, $query = null)
	{

		$model = get_called_class();

		if (is_null($query))
		{
			$query = $model::query();
		}

		if ($id == 'random')
   		{
   			$query->orderBy(DB::raw('RAND()'));
   		}
   		else
   		{
   			foreach ($model::$id_columns as $col)
   			{
   				$query->orWhere($col, '=', $id);
   			}
   		}
       	return $query->limit(1);
	}
}